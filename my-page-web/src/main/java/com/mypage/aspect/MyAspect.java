package com.mypage.aspect;

import com.mypage.commons.MypageUtiles;
import com.mypage.entity.BaseResult;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wdong
 * @version 1.0.0
 * @since 2023-03-07 20:53
 */

@Aspect
public class MyAspect {

    @Around("@within(org.springframework.web.bind.annotation.RestController)")
    public Object pageAspect(ProceedingJoinPoint joinPoint){
        ServletRequestAttributes servletRequest=(ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = servletRequest.getRequest();
        try {
            String pageSize=request.getParameter("pageSize");
            String pageNum=request.getParameter("pageNum");
            if(pageSize!=null&&pageNum!=null&&!pageSize.equals("")&&!pageNum.equals("")){
                if(Integer.parseInt(pageNum)>=1){
                    MypageUtiles.setPage(Integer.parseInt(pageNum),Integer.parseInt(pageSize));
                }
            }
            Object result = joinPoint.proceed();
            if(result instanceof BaseResult){
              ((BaseResult)result).setPage(MypageUtiles.getPage());
            }
            return result;
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }

    }
}
