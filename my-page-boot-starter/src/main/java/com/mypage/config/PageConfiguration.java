package com.mypage.config;

import com.mypage.aspect.MyAspect;
import com.mypage.plugin.MyPagePlugin;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wdong
 * @version 1.0.0
 * @since 2023-03-07 20:47
 */
@Configuration
public class PageConfiguration {

    @Bean
    @ConditionalOnProperty(prefix = "mypage.page",name="enable",havingValue = "true",matchIfMissing = true)
    public MyPagePlugin getMyPagePlugin(){
        System.out.println("mybatis加载。。。");
        return new MyPagePlugin();
    }

    @Bean
    @ConditionalOnWebApplication
    @ConditionalOnClass(MyPagePlugin.class)
    @ConditionalOnProperty(prefix = "mypage.web",name="enable",havingValue = "true",matchIfMissing = true)
    public MyAspect getMyAspect(){
        System.out.println("切面执行了！");
        return new MyAspect();
    }
}
