package com.mypage.entity;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author wdong
 * @version 1.0.0
 * @since 2023-03-07 18:59
 */
@Data
@Accessors(chain = true)
public class Page {

    private Integer pageNum;
    private Integer pageSize;
    private Integer pageMax;
}
