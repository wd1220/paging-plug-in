package com.mypage.commons;

import com.mypage.entity.Page;

/**
 * @author wdong
 * @version 1.0.0
 * @since 2023-03-07 18:59
 */
public class MypageUtiles {
    public static ThreadLocal<Page> threadLocal=new ThreadLocal<>();

    /**
     * 通过ThreadLocal设置page
     * @param pageNum
     * @param pageSize
     */
    public static void setPage(Integer pageNum,Integer pageSize){
        threadLocal.set(new Page().setPageNum(pageNum).setPageSize(pageSize));
    }

    /**
     * 获取page
     * @return
     */
    public static Page getPage(){
        return threadLocal.get();
    }

    /**
     * 移除ThreadLocal
     */
    public static void clear(){
        threadLocal.remove();
    }
}
