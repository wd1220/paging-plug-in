package com.mypage.plugin;

import com.mypage.commons.MypageUtiles;
import com.mypage.entity.Page;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;

import java.sql.*;

/**
 * @author wdong
 * @version 1.0.0
 * @since 2023-03-07 19:24
 */
@Intercepts(
        @Signature(
                type = StatementHandler.class,
                method = "prepare",
                args = {Connection.class,Integer.class}
        )
)
public class MyPagePlugin implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        //1、判断是否需要分页-ThreadLocal中获取Page对象
        Page page = MypageUtiles.getPage();
        if(page==null){
            //没有分页对象，不需要分页
            return invocation.proceed();
        }
        //2、获取sql
        //获取目标对象，有个bug!
        StatementHandler statementHandler = (StatementHandler)invocation.getTarget();
        BoundSql boundSql = statementHandler.getBoundSql();
        String sql = boundSql.getSql().trim().toLowerCase().replaceAll("/r/n"," ");
        System.out.println("【分页插件】：原生sql:"+sql);
        //sql全部转化为小写的去空格、换行的
        if(!sql.startsWith("select ")){
            //说明当前不是查询语句，无需分页
            return invocation.proceed();
        }
        
        if(sql.lastIndexOf(" limit ")!=-1){
            //说明当前已经存在分页，无需分页
            return invocation.proceed();
        }

        //查询总条数
        int count=countNum(sql,invocation);
        System.out.println("【分页插件】当前查询总条数："+count);
        int pageMaX=count% page.getPageSize()==0?count/ page.getPageSize():count/ page.getPageSize()+1;
        page.setPageMax(pageMaX);
        //改造原始SQL
        if(sql.endsWith(";")){
            sql=sql.substring(0,sql.length()-1);
        }
        sql+=" limit "+ (page.getPageNum()-1)* page.getPageSize()+","+ page.getPageSize();
        System.out.println("【分页插件】改造之后的分页SQL:" + sql);
        //将改造的sql 替换掉原始sql
        MetaObject metaObject = SystemMetaObject.forObject(boundSql);
        metaObject.setValue("sql",sql);//通过反射修改sql属性
        //放行
        return invocation.proceed();
    }

    /**
     * 获取当前查询的总条数
     * @param sql
     * @param invocation
     * @return
     */
    private Integer countNum(String sql, Invocation invocation) {

        StatementHandler statementHandler = (StatementHandler)invocation.getTarget();
        String sqlCount="select count(*) as total from ("+sql+") t";
        //获取当前的数据库连接
        System.out.println("拼接后的查询最大条数sql:"+sqlCount);
        Connection connection = (Connection) invocation.getArgs()[0];
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        try {
            statement = connection.prepareStatement(sqlCount);
            statementHandler.parameterize(statement);
            resultSet = statement.executeQuery();
            if(resultSet.next()){
                int total = resultSet.getInt("total");
                return total;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            if(resultSet!=null){
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
            if(statement!=null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return 0;
    }
}
